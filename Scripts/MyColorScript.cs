using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyColorScript : MonoBehaviour
{
    public GameObject sphere;
    private MeshRenderer renderer;

    private float rotateAmt = 1.0f;

    private void Start()
    {
        Debug.Log("Script Started");
    }

    private void Update()
    {
        sphere.transform.Rotate(0, rotateAmt, 0, Space.Self);
        // Debug.Log("rotateAmt: " + rotateAmt);
    }

    public void OnClickChangeColor()
    {
        renderer = sphere.GetComponent<MeshRenderer>();

        renderer.enabled = !renderer.enabled;
    }

    public void OnClickRenderEarth()
    {
        Debug.Log("Render Earth!");

        renderer = sphere.GetComponent<MeshRenderer>();

        // Load a single texture
        Texture2D texture = Resources.Load("Maps/nasa-earth-map") as Texture2D;

        renderer.material.mainTexture = texture;
    }
}
