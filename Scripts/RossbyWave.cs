using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RossbyWave : MonoBehaviour
{
    bool hemisphere; // true = north, false = south
    int wavenumber; // number of local maxima
    float phase; // shift in eastward direction, degrees of arc
    float latitude; // mean latitude of the rossby wave, positive is north
    float amplitude; // meridional distance from mean latitude to maximum
    float strength; // difference between temperatures poleward and equatorward of the polar front, Kelvins

    public RossbyWave(bool hemi, int waveN, float ph, float lat, float amp)
    {
        hemisphere = hemi;
        wavenumber = waveN;
        phase = ph;
        latitude = lat;
        amplitude = amp;
    }

    public bool[] markRossbyWave(List<Vector2> mapPoints) 
    {
        bool[] ret = new bool[mapPoints.Count];

        for(int i = 0; i < ret.Length; i++)
        {

        }

        return ret;
    }

    public void precessWave(float deltaT)
    {

    }
}
