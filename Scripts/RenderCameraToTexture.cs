using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderCameraToTexture : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Render Script Started");
    }


    public RenderTexture rt;
    // Update is called once per frame
    public void OnClickRender()
    {
        GetComponent<Camera>().RenderToCubemap(rt);
    }
}
