using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadEarthTextureAtStart : MonoBehaviour
{
    public GameObject sphere;
    private MeshRenderer renderer;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Earth Texture Renderer Started!");

        renderer = sphere.GetComponent<MeshRenderer>();

        // Load a single texture
        Texture2D texture = Resources.Load("Maps/nasa-earth-map") as Texture2D;

        renderer.material.mainTexture = texture;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
