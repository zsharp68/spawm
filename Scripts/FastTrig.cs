using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastTrig : MonoBehaviour
{
    // all x values are in degrees of arc

    float[] xLut;
    float[] ySin; //lookup tables
    float[] yAsin; //lookup tables

    private float res = 0;
    private float ares = 0;
    // does nearest-neighbor interpolation

    public FastTrig(int numEntries)
    {
        res = 90.0f/numEntries;
        ares = 1.0f/numEntries;

        int count = 0;

        xLut = new float[numEntries + 1];
        ySin = new float[numEntries + 1];
        yAsin = new float[numEntries + 1];
        for(float i = 0; i <= 90; i += 90.0f/numEntries)
        {
            xLut[count] = i;
            ySin[count] = Mathf.Sin(i * Mathf.Deg2Rad);
            yAsin[count] = Mathf.Asin(i/90.0f) * Mathf.Rad2Deg;

            count++;
        }
    }

    public float Sin(float x)
    {
        bool positive = (x >= 0);
        if(!positive) x *= -1;
        x %= 360;
        float x_ = x % 90;
        int xQ = (int) (x / 90);

        switch (xQ)
        {
            case 0:
                return ySin[(int) (x_/res)];
            case 1:
                return ySin[(int) ((90 - x_)/res)];
            case 2:
                return -ySin[(int) (x_/res)];
            case 3:
                return -ySin[(int) ((90 - x_)/res)];
            default:
                return 0;
        }
    }

    public float SinOld(float x)
    {
        bool positive = (x >= 0);
        if(!positive) x *= -1;
        x %= 360;
        float x_ = x % 90;
        int xQ = (int) (x / 90);

        int xI = -1;

        switch (xQ)
        {
            case 0:
                if(x_ == 0) 
                {
                    xI = 0;
                } 
                else if(x_ > xLut[xLut.Length - 1])
                {
                    xI = xLut.Length - 1;    
                }
                else 
                {
                    for(int i = 0; i < xLut.Length; i++)
                    {
                        if (x_ == xLut[i])
                        {
                            xI = i;
                            break; 
                        }
                        else if (x_ > xLut[i] && x_ < xLut[i + 1])
                        {
                            if((x_ - xLut[i]) < (xLut[i + 1] - x_))
                            {
                                xI = i;
                                break; 
                            }
                            else
                            {
                                xI = i + 1;
                                break; 
                            }
                        }
                    }
                }

                return ySin[xI];

            case 1:
                x_ = 90 - x_;

                if(x_ == 0) 
                {
                    xI = 0;
                } 
                else if(x_ > xLut[xLut.Length - 1])
                {
                    xI = xLut.Length - 1;    
                }
                else 
                {
                    for(int i = 0; i < xLut.Length; i++)
                    {
                        if (x_ == xLut[i])
                        {
                            xI = i;
                            break; 
                        }
                        else if (x_ > xLut[i] && x_ < xLut[i + 1])
                        {
                            if((x_ - xLut[i]) < (xLut[i + 1] - x_))
                            {
                                xI = i;
                                break; 
                            }
                            else
                            {
                                xI = i + 1;
                                break; 
                            }
                        }
                    }
                }

                return ySin[xI];
            
            case 2:
                if(x_ == 0) 
                {
                    xI = 0;
                } 
                else if(x_ > xLut[xLut.Length - 1])
                {
                    xI = xLut.Length - 1;    
                }
                else 
                {
                    for(int i = 0; i < xLut.Length; i++)
                    {
                        if (x_ == xLut[i])
                        {
                            xI = i;
                            break; 
                        }
                        else if (x_ > xLut[i] && x_ < xLut[i + 1])
                        {
                            if((x_ - xLut[i]) < (xLut[i + 1] - x_))
                            {
                                xI = i;
                                break; 
                            }
                            else
                            {
                                xI = i + 1;
                                break; 
                            }
                        }
                    }
                }

                return ySin[xI];

            case 3:
                x_ = 90 - x_;

                if(x_ == 0) 
                {
                    xI = 0;
                } 
                else if(x_ > xLut[xLut.Length - 1])
                {
                    xI = xLut.Length - 1;   
                }
                else 
                {
                    for(int i = 0; i < xLut.Length - 1; i++)
                    {
                        if (x_ == xLut[i])
                        {
                            xI = i;
                            break; 
                        }
                        else if (x_ > xLut[i] && x_ < xLut[i + 1])
                        {
                            if((x_ - xLut[i]) < (xLut[i + 1] - x_))
                            {
                                xI = i;
                                break; 
                            }
                            else
                            {
                                xI = i + 1;
                                break; 
                            }
                        }
                    }
                }

                return ySin[xI];

            default:
                return 0;
        }
    }

    public float Cos(float x)
    {
        return Sin(x + 90);
    }

    public float Tan(float x)
    {
        return Sin(x)/Cos(x);
    }

    public float Csc(float x)
    {
        return 1/Sin(x);
    }

    public float Sec(float x)
    {
        return 1/Cos(x);
    }

    public float Cot(float x)
    {
        return Cos(x)/Sin(x);
    }
    
    public float Asin(float x)
    {
        if (x >= 0)
        {
            return yAsin[(int) (x/ares)];
        }
        else
        {
            return Asin(-x);
        }
    }

    public float AsinOld(float x)
    {
        if (x >= 0)
        {
            int yI = -1;

            if (x == 0) 
            {
                return 0;
            }
            else if (x >= ySin[ySin.Length - 1])
            {
                return ySin[ySin.Length - 1];
            }
            else
            {
                for (int i = 0; i < ySin.Length - 1; i++)
                {
                    if (x == ySin[i])
                    {
                        yI = i;
                        break;
                    }
                    else if (x > ySin[i] && x < ySin[i + 1])
                    {
                        if((x - ySin[i]) < (ySin[i + 1] - x))
                        {
                            yI = i;
                            break;
                        }
                        else
                        {
                            yI = i + 1;
                            break;
                        }
                    }
                }
            }

            return xLut[yI];
        }
        else
        {
            return -Asin(-x);
        }
    }

    public float Acos(float x)
    {
        return 0.5f * Mathf.PI - Asin(x);
    }
}
