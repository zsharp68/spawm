using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherModel : MonoBehaviour
{
    public List<Vector2> mappingScheme = new List<Vector2>(); // done to allow total flexibility for whatever mecha wants
    public List<float> mappingSchemeTileSize = new List<float>(); // for computation of weighted averages such as to compute average planetary tmeperature
    Vector3 stellarDistanceVector = new Vector3(1, 0, 0); // normalized distance vector, +X:0W, +Y:90N +Z:90W

    public List<float> potentialTemperature = new List<float>(); // kelvins
    // remember that this is the idealized potential temperature. true temperature is adjusted by elevation
    // maybe have separate surface and air temperature
    public List<float> sst = new List<float>(); // exponential moving average of the air temperature, kelvins
    public float sstConstant = 0.001f; // the change of the sea surface temperature towards the air temperature per hour
    public List<float> wind = new List<float>(); // m/s
    public List<float> seaIce = new List<float>(); // fraction, decided by function of sea surface temperature (-1 means land)
    public List<float> seaIceMeltPonds = new List<float>(); // fraction, parameterizes the reduction of sea ice albedo by melt ponds
    public float meltRatio = 0.0005f; // melt pond growth per Wh/m^2 of stellar energy in
    public float refreezeRatio = 0.0005f; // melt pond shrinkage per Wh/m^2 of stellar energy in // next try adding another 0
    public List<float> precipRate = new List<float>(); // mm/s or dBZ, undecided
    public List<float> lowClouds = new List<float>(); // either do as cloud water content or as a transparency percentage
    public List<float> highClouds = new List<float>(); // either do as cloud water content or as a transparency percentage
    public List<float> stratosphericAsh = new List<float>(); // maybe a transparency percentage
    public List<float> powerIn = new List<float>(); // W/m^2
    public List<float> powerOut = new List<float>(); // W/m^2
    public List<float> albedo = new List<float>(); //  percentage/100, 1 = reflects all
    List<float> emissivity = new List<float>(); //  percentage/100, 1 = emits all
    List<float> specificHeat = new List<float>(); //  (joules/kilogram)/kelvin
    List<float> surfaceDensity = new List<float>(); //  kilograms/meter^3
    List<float> specificHeatArea = new List<float>(); //  (joules/meter^2)/kelvin
    List<int> landSeaMask = new List<int>(); //  sea = 0, land = 1;
    public List<float> elevation = new List<float>(); //  meters above sea level
    List<Vector3> surfaceNormal = new List<Vector3>(); //  meters above sea level
    float planetaryRadius = 0; // km
    float rotationPeriod = 0; // hours
    float stellarDistance = 0; // millions of km
    float stellarLuminosity = 0; // solar luminosities
    float ingoingAtmoTransp = 0; // percentage/100
    float outgoingAtmoTransp = 0; // percentage/100
    float atmosphericThickness = 0.0009881f;
    float avgAtmosThick = 0;

    int atmoLutSize = 900;
    float[] atmoLut;

    private static readonly int ftSize = 900;
    FastTrig ft = new FastTrig(ftSize);

    public WeatherModel()
    {
        atmoLut = new float[atmoLutSize + 1];

        for(int i = 0; i < atmoLutSize + 1; i++)
        {
            atmoLut[i] = AtmosphereAmt((float) (i)*(90.0f/atmoLutSize));
        }

        avgAtmosThick = AtmosphereAmtLUT(63.6396103068f);

        List<Vector2> mapSch = new List<Vector2>();

        for(int i = -179; i <= 180; i++)
        {
            for(int j = -90; j <= 90; j++)
            {
                mapSch.Add(new Vector2(i, j));
                mappingSchemeTileSize.Add(Mathf.Cos(j*Mathf.Deg2Rad));
            }
        }

        mappingScheme = mapSch;
        stellarDistance = 149.6f;
        stellarLuminosity = 1.0f;
        ingoingAtmoTransp = 0.77379553466f;
        outgoingAtmoTransp = 0.522461074837f; // normally 0.60246...

        float luminosityEW = 382800.0f * stellarLuminosity; // exawatts
        float radiantIntensity = luminosityEW / (4 * Mathf.PI); // exawatts/steradian
        float surfaceAreaStDistance = 4 * Mathf.PI * (stellarDistance * stellarDistance); // Gigameters^2
        float maxInsolation = 1000.0f*luminosityEW/surfaceAreaStDistance;

        for(int i = 0; i < mappingScheme.Count; i++) 
        {
            if(mappingScheme[i].x <= 0) 
            {
                albedo.Add(0.2f);
                emissivity.Add(0.96f);
                specificHeat.Add(840);
                surfaceDensity.Add(1600.0f);

                float equilibriumTemp = Mathf.Pow(maxInsolation*(1-albedo[i])/(4*0.0000000567f), 0.25f);
                potentialTemperature.Add(equilibriumTemp);
                sst.Add(equilibriumTemp);
                powerIn.Add(0);
                powerOut.Add(0);

                surfaceNormal.Add(new Vector3(Mathf.Cos(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad)));

                landSeaMask.Add(1);
                elevation.Add(0.1f);

                specificHeatArea.Add(specificHeat[i]*surfaceDensity[i]*energyScatterDepth);
            } 
            else
            {
                albedo.Add(0.06f);
                emissivity.Add(0.96f);
                specificHeat.Add(4184.0f);
                surfaceDensity.Add(1000.0f);

                float equilibriumTemp = Mathf.Pow(0.25f*maxInsolation/(0.0000000567f*(1-albedo[i])), 0.25f);
                potentialTemperature.Add(equilibriumTemp);
                sst.Add(equilibriumTemp);
                powerIn.Add(0);
                powerOut.Add(0);

                surfaceNormal.Add(new Vector3(Mathf.Cos(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad)));

                landSeaMask.Add(0);
                elevation.Add(-0.1f);

                specificHeatArea.Add(specificHeat[i]*surfaceDensity[i]*energyScatterDepth);
            }

            if(sst[i] > 275.15)
            {
                seaIce.Add(0);
            }
            else if(sst[i] < 271.15)
            {
                seaIce.Add(1);
            }
            else
            {
                seaIce.Add(-0.25f * (sst[i] - 271.15f) + 1);
            }

            seaIceMeltPonds.Add(0);

            if(landSeaMask[i] == 0) albedo[i] = seaIce[i] * 0.5f + (1 - seaIce[i]) * 0.06f;
        }
    }

    public WeatherModel(List<Vector2> mapSch, List<float> tileSize)
    {
        atmoLut = new float[atmoLutSize + 1];

        for(int i = 0; i < atmoLutSize + 1; i++)
        {
            atmoLut[i] = AtmosphereAmt((float) (i)*(90.0f/atmoLutSize));
        }

        avgAtmosThick = AtmosphereAmtLUT(63.6396103068f);
        
        mappingScheme = mapSch;
        mappingSchemeTileSize = tileSize;
        stellarDistance = 149.6f;
        stellarLuminosity = 1;
        ingoingAtmoTransp = 0.77379553466f;
        outgoingAtmoTransp = 0.602461074837f;

        for(int i = 0; i < mappingScheme.Count; i++) 
        {
            albedo.Add(0.2f);
            emissivity.Add(0.96f);
            specificHeat.Add(840);
            surfaceDensity.Add(1600.0f);
            potentialTemperature.Add(0);
            sst.Add(0);
            powerIn.Add(0);
            powerOut.Add(0);

            surfaceNormal.Add(new Vector3(Mathf.Cos(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad)));

            landSeaMask.Add(1);
            elevation.Add(0.1f);

            specificHeatArea.Add(specificHeat[i]*surfaceDensity[i]*energyScatterDepth);

            if(sst[i] > 275.15)
            {
                seaIce.Add(0);
            }
            else if(sst[i] < 271.15)
            {
                seaIce.Add(1);
            }
            else
            {
                seaIce.Add(-0.25f * (sst[i] - 271.15f) + 1);
            }

            seaIceMeltPonds.Add(0);

            if(landSeaMask[i] == 0) 
            {
                albedo[i] = seaIce[i] * 0.5f + (1 - seaIce[i]) * 0.06f;
            }
        }
    }

    public WeatherModel(List<Vector2> mapSch, List<float> tileSize, float sDist, float sLumin)
    {
        atmoLut = new float[atmoLutSize + 1];

        for(int i = 0; i < atmoLutSize + 1; i++)
        {
            atmoLut[i] = AtmosphereAmt((float) (i)*(90.0f/atmoLutSize));
        }
        
        avgAtmosThick = AtmosphereAmtLUT(63.6396103068f);
        
        mappingScheme = mapSch;
        mappingSchemeTileSize = tileSize;
        stellarDistance = sDist;
        stellarLuminosity = sLumin;
        ingoingAtmoTransp = 0.77379553466f;
        outgoingAtmoTransp = 0.602461074837f;

        for(int i = 0; i < mappingScheme.Count; i++) 
        {
            albedo.Add(0.2f);
            emissivity.Add(0.96f);
            specificHeat.Add(840);
            surfaceDensity.Add(1600.0f);
            potentialTemperature.Add(0);
            sst.Add(0);
            powerIn.Add(0);
            powerOut.Add(0);

            surfaceNormal.Add(new Vector3(Mathf.Cos(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad)));

            landSeaMask.Add(1);
            elevation.Add(0.1f);

            specificHeatArea.Add(specificHeat[i]*surfaceDensity[i]*energyScatterDepth);

            if(sst[i] > 275.15)
            {
                seaIce.Add(0);
            }
            else if(sst[i] < 271.15)
            {
                seaIce.Add(1);
            }
            else
            {
                seaIce.Add(-0.25f * (sst[i] - 271.15f) + 1);
            }

            seaIceMeltPonds.Add(0);

            if(landSeaMask[i] == 0) albedo[i] = seaIce[i] * 0.5f + (1 - seaIce[i]) * 0.06f;
        }
    }

    public WeatherModel(List<Vector2> mapSch, List<float> tileSize, List<float> elev, float sDist, float sLumin)
    {
        atmoLut = new float[atmoLutSize + 1];

        for(int i = 0; i < atmoLutSize + 1; i++)
        {
            atmoLut[i] = AtmosphereAmt((float) (i)*(90.0f/atmoLutSize));
        }
        
        avgAtmosThick = AtmosphereAmtLUT(63.6396103068f);
        
        mappingScheme = mapSch;
        mappingSchemeTileSize = tileSize;
        stellarDistance = sDist;
        stellarLuminosity = sLumin;
        ingoingAtmoTransp = 0.77379553466f;
        outgoingAtmoTransp = 0.602461074837f;

        elevation = elev;

        for(int i = 0; i < mappingScheme.Count; i++) 
        {
            potentialTemperature.Add(0);
            sst.Add(0);
            powerIn.Add(0);
            powerOut.Add(0);

            surfaceNormal.Add(new Vector3(Mathf.Cos(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad)));

            if(elevation[i] <= 0)
            {
                landSeaMask.Add(0);
                albedo.Add(0.06f);
                emissivity.Add(0.96f);
                specificHeat.Add(4184.0f);
                surfaceDensity.Add(1000.0f);
            } 
            else {
                landSeaMask.Add(1);
                albedo.Add(0.2f);
                emissivity.Add(0.96f);
                specificHeat.Add(840.0f);
                surfaceDensity.Add(1600.0f);
            }

            specificHeatArea.Add(specificHeat[i]*surfaceDensity[i]*energyScatterDepth);

            if(sst[i] > 275.15)
            {
                seaIce.Add(0);
            }
            else if(sst[i] < 271.15)
            {
                seaIce.Add(1);
            }
            else
            {
                seaIce.Add(-0.25f * (sst[i] - 271.15f) + 1);
            }

            seaIceMeltPonds.Add(0);

            if(landSeaMask[i] == 0) albedo[i] = seaIce[i] * 0.5f + (1 - seaIce[i]) * 0.06f;
        }
    }

    public WeatherModel(List<Vector2> mapSch, List<float> tileSize, List<float> elev, List<int> lsMask, float sDist, float sLumin)
    {
        atmoLut = new float[atmoLutSize + 1];

        for(int i = 0; i < atmoLutSize + 1; i++)
        {
            atmoLut[i] = AtmosphereAmt((float) (i)*(90.0f/atmoLutSize));
        }
        
        avgAtmosThick = AtmosphereAmtLUT(63.6396103068f);
        
        mappingScheme = mapSch;
        mappingSchemeTileSize = tileSize;
        stellarDistance = sDist;
        stellarLuminosity = sLumin;
        ingoingAtmoTransp = 0.77379553466f;
        outgoingAtmoTransp = 0.602461074837f;

        elevation = elev;
        landSeaMask = lsMask;

        for(int i = 0; i < mappingScheme.Count; i++) 
        {
            potentialTemperature.Add(0);
            sst.Add(0);
            powerIn.Add(0);
            powerOut.Add(0);

            surfaceNormal.Add(new Vector3(Mathf.Cos(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad)));

            if(lsMask[i] == 0)
            {
                albedo.Add(0.06f);
                emissivity.Add(0.96f);
                specificHeat.Add(4184.0f);
                surfaceDensity.Add(1000.0f);
            } 
            else {
                albedo.Add(0.2f);
                emissivity.Add(0.96f); // later, maybe use 0.70 for desert, 0.8 for semi arid
                specificHeat.Add(840.0f);
                surfaceDensity.Add(1600.0f);
            }

            specificHeatArea.Add(specificHeat[i]*surfaceDensity[i]*energyScatterDepth);

            if(sst[i] > 275.15)
            {
                seaIce.Add(0);
            }
            else if(sst[i] < 271.15)
            {
                seaIce.Add(1);
            }
            else
            {
                seaIce.Add(-0.25f * (sst[i] - 271.15f) + 1);
            }

            seaIceMeltPonds.Add(0);

            if(lsMask[i] == 0) albedo[i] = seaIce[i] * 0.5f + (1 - seaIce[i]) * 0.06f;
        }
    }

    private int testIndex = -1;
    private float energyScatterDepth = 0.7f; // meters

    private float conductionConstant = 0.008f;

    int threadGroupSize = 64;

    private bool modelStarted = false;
            
    public ComputeShader shader;
    int stlHKernel;
    int radCKernel;
    int hTraKernel;
    public ComputeBuffer bufferNorm;
    public ComputeBuffer bufferLndS;
    public ComputeBuffer bufferIce;
    public ComputeBuffer bufferTemp;
    public ComputeBuffer bufferAlb;
    public ComputeBuffer bufferEmsv;
    public ComputeBuffer bufferPwrI;
    public ComputeBuffer bufferPwrO;
    public ComputeBuffer bufferSpfH;
    public void Step(float deltaT, Vector3 stDistVector, float stDist)
    {
        if(!modelStarted)
        {
            modelStarted = true;

            // load shader, try to move to some start method
            shader = Resources.Load<ComputeShader>("Shaders/WxModelShaders");
            stlHKernel = shader.FindKernel("StellarHeating");
            radCKernel = shader.FindKernel("RadiationalCooling");
            hTraKernel = shader.FindKernel("HeatTropicsToPoles");

            int bufferSize = surfaceNormal.Count;

            // allocate shader buffers
            bufferNorm = new ComputeBuffer(bufferSize, 3 * sizeof(float));
            bufferLndS = new ComputeBuffer(bufferSize, sizeof(int));
            bufferIce = new ComputeBuffer(bufferSize, sizeof(float));
            bufferTemp = new ComputeBuffer(bufferSize, sizeof(float));
            bufferAlb = new ComputeBuffer(bufferSize, sizeof(float));
            bufferEmsv = new ComputeBuffer(bufferSize, sizeof(float));
            bufferPwrI = new ComputeBuffer(bufferSize, sizeof(float));
            bufferPwrO = new ComputeBuffer(bufferSize, sizeof(float));
            bufferSpfH = new ComputeBuffer(bufferSize, sizeof(float));

            // set shader constants
            shader.SetFloat("avgAtmosThick", avgAtmosThick);
            shader.SetFloat("ingoingAtmoTransp", ingoingAtmoTransp);
            shader.SetFloat("outgoingAtmoTransp", outgoingAtmoTransp);
            shader.SetFloat("conductionConstant", conductionConstant);

            // set shader LUTs
            shader.SetFloat("atmosphericThickness", atmosphericThickness);
            shader.SetFloat("atmoLutSize", atmoLutSize);
            ComputeBuffer bufferAtmoLUT = new ComputeBuffer(atmoLut.Length, sizeof(float));
            bufferAtmoLUT.SetData(atmoLut);
            shader.SetBuffer(stlHKernel, "atmoThickLUT", bufferAtmoLUT);
            shader.SetBuffer(radCKernel, "atmoThickLUT", bufferAtmoLUT);
            
            // load in buffers with constant fields
            bufferNorm.SetData(surfaceNormal);
            bufferLndS.SetData(landSeaMask);
            bufferIce.SetData(seaIce);
            bufferAlb.SetData(albedo);
            bufferEmsv.SetData(emissivity);
            bufferPwrI.SetData(powerIn);
            bufferPwrO.SetData(powerOut);
            bufferSpfH.SetData(specificHeatArea);

            shader.SetBuffer(stlHKernel, "normalVector", bufferNorm);
            shader.SetBuffer(stlHKernel, "landSeaMask", bufferLndS);
            shader.SetBuffer(stlHKernel, "seaIce", bufferIce);
            shader.SetBuffer(stlHKernel, "albedo", bufferAlb);
            shader.SetBuffer(stlHKernel, "powerIn", bufferPwrI);
            shader.SetBuffer(stlHKernel, "powerOut", bufferPwrO);
            shader.SetBuffer(stlHKernel, "specificHeatArea", bufferSpfH);
            shader.SetBuffer(radCKernel, "normalVector", bufferNorm);
            shader.SetBuffer(radCKernel, "landSeaMask", bufferLndS);
            shader.SetBuffer(radCKernel, "emissivity", bufferEmsv);
            shader.SetBuffer(radCKernel, "powerIn", bufferPwrI);
            shader.SetBuffer(radCKernel, "powerOut", bufferPwrO);
            shader.SetBuffer(radCKernel, "specificHeatArea", bufferSpfH);
        }

        // do stellar irradiation
        float luminosityEW = 382800.0f * stellarLuminosity; // exawatts
        float radiantIntensity = luminosityEW / (4 * Mathf.PI); // exawatts/steradian
        float surfaceAreaStDistance = 4 * Mathf.PI * (stDist * stDist); // Gigameters^2
        float maxInsolation = 1000.0f*luminosityEW/surfaceAreaStDistance;

        shader.SetFloat("maxInsolation", maxInsolation);
        shader.SetFloat("stDistVectorX", stDistVector.x);
        shader.SetFloat("stDistVectorY", stDistVector.y);
        shader.SetFloat("stDistVectorZ", stDistVector.z);
        shader.SetFloat("deltaT", deltaT);
        int numThreadGroups = potentialTemperature.Count / threadGroupSize;

        bufferTemp.SetData(potentialTemperature);
        bufferAlb.SetData(albedo);
        bufferIce.SetData(seaIce);
        bufferSpfH.SetData(specificHeatArea);

        shader.SetBuffer(stlHKernel, "temperature", bufferTemp);
        shader.SetBuffer(stlHKernel, "albedo", bufferAlb);
        shader.SetBuffer(stlHKernel, "specificHeatArea", bufferSpfH);
        shader.SetBuffer(stlHKernel, "seaIce", bufferIce);

        shader.Dispatch(stlHKernel, numThreadGroups, 1, 1);

        float[] potentialTemperatureArr = potentialTemperature.ToArray();
        float[] powerInArr = powerIn.ToArray();

        bufferTemp.GetData(potentialTemperatureArr);
        bufferPwrI.GetData(powerInArr);
        
        potentialTemperature = new List<float>(potentialTemperatureArr);
        powerIn = new List<float>(powerInArr);

        for(int i = potentialTemperature.Count - (potentialTemperature.Count % threadGroupSize); i < potentialTemperature.Count; i++)
        {
            float normalDotDistance = surfaceNormal[i].x * stDistVector.x + surfaceNormal[i].y * stDistVector.y + surfaceNormal[i].z * stDistVector.z;
            
            float atmosThick = AtmosphereAmtLUT(Mathf.Asin(normalDotDistance)*Mathf.Rad2Deg);
            float insolationTransmitted = maxInsolation*Mathf.Pow(ingoingAtmoTransp, atmosThick/avgAtmosThick);
            
            float insolationAtSurface = insolationTransmitted * ((normalDotDistance > 0)?normalDotDistance:0);

            float insolationAtSurfaceAbsorbed = insolationAtSurface * (1 - albedo[i]);
            if(landSeaMask[i] == 0)
            {
                insolationAtSurfaceAbsorbed = insolationAtSurface * (1 - OceanReflectance(Mathf.Asin(normalDotDistance)*Mathf.Rad2Deg));
            }
            powerIn[i] = insolationAtSurfaceAbsorbed;

            float insolationJoules = insolationAtSurfaceAbsorbed * deltaT;

            float deltaTemp = insolationJoules / specificHeatArea[i]; // kelvins
            
            potentialTemperature[i] += deltaTemp;
        }

        // do radiational cooling
        bufferTemp.SetData(potentialTemperature);
        bufferEmsv.SetData(emissivity);

        shader.SetBuffer(radCKernel, "temperature", bufferTemp);
        shader.SetBuffer(radCKernel, "emissivity", bufferEmsv);
        shader.SetBuffer(radCKernel, "specificHeatArea", bufferSpfH);

        shader.Dispatch(radCKernel, numThreadGroups, 1, 1);

        potentialTemperatureArr = potentialTemperature.ToArray();
        float[] powerOutArr = powerOut.ToArray();

        bufferTemp.GetData(potentialTemperatureArr);
        bufferPwrO.GetData(powerOutArr);
        
        potentialTemperature = new List<float>(potentialTemperatureArr);
        powerOut = new List<float>(powerOutArr);

        for(int i = potentialTemperature.Count - (potentialTemperature.Count % threadGroupSize); i < potentialTemperature.Count; i++)
        {
            float sigma = 0.0000000567f;
            float T = potentialTemperature[i];

            float radiationalPower = emissivity[i]*outgoingAtmoTransp*sigma*T*T*T*T;
            float energyRadiated = radiationalPower*deltaT;
            powerOut[i] = radiationalPower;

            float specificHeatArea = surfaceDensity[i] * specificHeat[i] * energyScatterDepth;
            float deltaTemp = -energyRadiated/specificHeatArea;
            
            potentialTemperature[i] += deltaTemp; 
        }

        // spread out heat to all over planet (preliminary heat transport from tropics to poles)

        float avgTemp = PlanetaryAverage(potentialTemperature);
        float avgSpfH = PlanetaryAverage(specificHeatArea);

        float sstConstantDT = 1-Mathf.Pow(1-sstConstant, deltaT/3600.0f); // adjusts for delta-T
        float conductionConstantDT = 1-Mathf.Pow(1-conductionConstant, deltaT/3600.0f);
        for(int i = 0; i < potentialTemperature.Count; i++)
        {
            // multiply conduction constant with (specificHeatArea/avgSpecificHeatArea)
            float localConductionConstant = conductionConstantDT / Mathf.Pow(specificHeatArea[i]/avgSpfH, 0.5f);
            potentialTemperature[i] = (1 - localConductionConstant) * potentialTemperature[i] + localConductionConstant * avgTemp;

            sst[i] = (1 - sstConstantDT) * sst[i] + sstConstantDT * potentialTemperature[i];

            if(sst[i] > 275.15)
            {
                seaIce[i] = 0;
            }
            else if(sst[i] < 271.15)
            {
                seaIce[i] = 1;
            }
            else
            {
                //if(seaIce[i] < -0.25f * (sst[i] - 271.15f) + 1) seaIceMeltPonds[i] = 0;
                seaIce[i] = -0.25f * (sst[i] - 271.15f) + 1;
            }

            seaIceMeltPonds[i] += (powerIn[i] * meltRatio - powerOut[i] * refreezeRatio) * deltaT/3600.0f;
            if(seaIceMeltPonds[i] > 1) seaIceMeltPonds[i] = 1;
            if(seaIceMeltPonds[i] < 0) seaIceMeltPonds[i] = 0;

            if(landSeaMask[i] == 0) 
            {
                float effectiveSeaIce = seaIce[i] * (1 - seaIceMeltPonds[i]);
                albedo[i] = effectiveSeaIce * 0.5f + (1 - effectiveSeaIce) * 0.06f;
                emissivity[i] = seaIce[i] * 0.96f + (1 - seaIce[i]) * 0.96f;
                specificHeatArea[i] = seaIce[i] * 1937252.0f + (1 - seaIce[i]) * 4184000.0f;
            }
        }

        //Debug.Log("Stepped Forward by " + deltaT + " seconds");
    }

    public void Step(float deltaT, Vector3 stDistVector)
    {
        Step(deltaT, stDistVector, stellarDistance);
    }

    private float PlanetaryAverage(List<float> field)
    {
        float uwValuesSum = 0.0f;
        float valuesSum = 0.0f;
        float weightSum = 0.0f;

        for(int i = 0; i < field.Count; i++)
        {
            uwValuesSum += field[i];
            valuesSum += field[i] * mappingSchemeTileSize[i];
            weightSum += mappingSchemeTileSize[i];
        }

        // Debug.Log("uwAvg:" + uwValuesSum/field.Count);
        // Debug.Log("valuesSum:" + valuesSum);
        // Debug.Log("weightSum:" + weightSum);
        // Debug.Log("weightedAvg:" + (valuesSum/weightSum));

        return valuesSum/weightSum;
    }

    public void StepOld(float deltaT, Vector3 stDistVector, float stDist)
    {
        long insolTB = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        // do stellar irradiation
        for(int i = 0; i < mappingScheme.Count; i++)
        {
            //Debug.Log("Start Irradiation Step");

            // find test index based on lon-lat pair
            if(mappingScheme[i].x == -91 && mappingScheme[i].y == 33)
            {
                testIndex = i;
                //Debug.Log("Test Index: " + i);
            }

            // convert degrees to radians here
            Vector3 normalVector = new Vector3(Mathf.Cos(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].y*Mathf.Deg2Rad), Mathf.Sin(mappingScheme[i].x*Mathf.Deg2Rad) * Mathf.Cos(mappingScheme[i].y*Mathf.Deg2Rad));
            float normalDotDistance = normalVector.x * stDistVector.x + normalVector.y * stDistVector.y + normalVector.z * stDistVector.z;

            //figure out how to turn luminosity and distance into W/m^2 irradiance value
            float luminosityEW = 382800.0f * stellarLuminosity; // exawatts
            float radiantIntensity = luminosityEW / (4 * Mathf.PI); // exawatts/steradian
            float surfaceAreaStDistance = 4 * Mathf.PI * (stDist * stDist); // Gigameters^2

            //if(i == testIndex) Debug.Log("Max Insolation: " + 1000.0f*luminosityEW/surfaceAreaStDistance + " W/m^2");
            
            float atmosThick = AtmosphereAmt(Mathf.Asin(normalDotDistance)*Mathf.Rad2Deg);
            float avgAtmosThick = AtmosphereAmt(63.6396103068f);
            float insolationTransmitted = 1000.0f*Mathf.Pow(ingoingAtmoTransp, atmosThick/avgAtmosThick)*luminosityEW/surfaceAreaStDistance;
            
            //if(i == testIndex) Debug.Log("Max Insolation (transmitted): " + insolationTransmitted + " W/m^2");
            
            float insolationAtSurface = insolationTransmitted * ((normalDotDistance > 0)?normalDotDistance:0);
            
            //if(i == testIndex) Debug.Log("Longitude, Latitude: " + mappingScheme[i].x + ", " + mappingScheme[i].y);
            //if(i == testIndex) Debug.Log("Distance Vector: " + stDistVector.x + ", " + stDistVector.y + ", " + stDistVector.z);
            //if(i == testIndex) Debug.Log("Normal Vector: " + normalVector.x + ", " + normalVector.y + ", " + normalVector.z);
            //if(i == testIndex) Debug.Log("Normal . Distance: " + normalDotDistance);
            //if(i == testIndex) Debug.Log("Insolation At Surface: " + insolationAtSurface + " W/m^2");

            float insolationAtSurfaceAbsorbed = insolationAtSurface * (1 - albedo[i]);
            powerIn[i] = insolationAtSurfaceAbsorbed;

            //if(i == testIndex) Debug.Log("1 - Albedo: " + (1 - albedo[i]));
            //if(i == testIndex) Debug.Log("Insolation Power Absorbed: " + insolationAtSurfaceAbsorbed + " W/m^2");

            float insolationJoules = insolationAtSurfaceAbsorbed * deltaT;
            
            //if(i == testIndex) Debug.Log("Energy Absorbed: " + insolationJoules + " J/m^2");
            
            //assumes all energy is equally scattered 1.0 meters deep for now
            // if(i == testIndex) Debug.Log("Specific Heat: " + specificHeat[i] + " J/(kg*K)");
            // if(i == testIndex) Debug.Log("Surface Density: " + surfaceDensity[i] + " kg/(m^3)");
            // if(i == testIndex) Debug.Log("Specific Heat (Area): " + (surfaceDensity[i] * specificHeat[i] * energyScatterDepth) + " J/(m^2*K)");
        
            float specificHeatArea = surfaceDensity[i] * specificHeat[i] * energyScatterDepth;

            float deltaTemp = insolationJoules / specificHeatArea; // kelvins

            //if(i == testIndex) Debug.Log("Change in Temperature: " + deltaTemp + " K");
            
            potentialTemperature[i] += deltaTemp;

            //if(i == testIndex) Debug.Log("Temperature: " + potentialTemperature[i] + " K");
        }
        long insolTA = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        
        long insolThickTB = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        for(int i = 0; i < mappingScheme.Count; i++)
        {
            float atmosThick = AtmosphereAmt(Mathf.Asin(0.13463f)*Mathf.Rad2Deg);
            float avgAtmosThick = AtmosphereAmt(63.6396103068f);         
        }
        long insolThickTA = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

        long radCoolTB = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        // do radiational cooling
        for(int i = 0; i < mappingScheme.Count; i++)
        {
            float emissivity = (1 - albedo[i]);
            float sigma = 0.0000000567f;
            float T = potentialTemperature[i];
            
            float radiationalPower = emissivity*outgoingAtmoTransp*sigma*T*T*T*T;
            float energyRadiated = radiationalPower*deltaT;
            powerOut[i] = radiationalPower;

            //if(i == testIndex) Debug.Log("Radiational Power: " + radiationalPower + " W/m^2");
            //if(i == testIndex) Debug.Log("Energy Radiated: " + energyRadiated + " J/m^2");

            float specificHeatArea = surfaceDensity[i] * specificHeat[i] * energyScatterDepth;
            float deltaTemp = -energyRadiated/specificHeatArea;

            //if(i == testIndex) Debug.Log("Change in Temperature: " + deltaTemp + " K");
            
            potentialTemperature[i] += deltaTemp;

            //if(i == testIndex) Debug.Log("Temperature: " + potentialTemperature[i] + " K");
        }
        long radCoolTA = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        Debug.Log("Insolation: " + (insolTA - insolTB) + " ms");
        Debug.Log("\tAtmospheric Thickness: " + (insolThickTA - insolThickTB) + " ms");
        Debug.Log("Radiational Cooling: " + (radCoolTA - radCoolTB) + " ms");

        //Debug.Log("Stepped Forward by " + deltaT + " seconds");
    }

    public void StepOld(float deltaT, Vector3 stDistVector)
    {
        StepOld(deltaT, stDistVector, stellarDistance);
    }

    // elevation: degrees
    // atmoRad: planetary radii
    private float AtmosphereAmt(float elevation)
    {
        return AtmosphereAmt(elevation, atmosphericThickness);
    }

    // elevation: degrees
    // atmoRad: planetary radii
    private float AtmosphereAmt(float elev, float atmoRad)
    {
        float elevation = 90 - elev;

        float tauX = (1 + atmoRad)*Mathf.Cos(Mathf.Asin((1/(1+atmoRad)*Mathf.Sin(elevation*Mathf.Deg2Rad))));
        float pX = Mathf.Cos(elevation*Mathf.Deg2Rad);

        return tauX - pX;
    }

    // angle: angular distance between sun and zenith, degrees of arc
    private float AtmosphereAmtLUT(float angle)
    {
        if(angle < 0)
        {
            return 0;
        }
        else if(angle >= 90)
        {
            return atmosphericThickness;
        }
        else
        {
            // Debug.Log("Angle: " + angle);
            try
            {
                return atmoLut[(int) ((angle + 45.0f/atmoLutSize)/(90.0f/atmoLutSize))];
            }
            catch (Exception e)
            {
                Debug.Log(angle);
                Debug.Log((int) ((angle + 45.0f/atmoLutSize)/(90.0f/atmoLutSize)));
            }

            throw new IndexOutOfRangeException();
        }
    }

    // angle: angular distance between sun and zenith, degrees of arc
    // returns amount of light reflected off the water
    private float OceanReflectance(float elevation)
    {
        if(elevation > 50.0f)
        {
            return 0.03f;
        } 
        else if(elevation > 40.0f)
        {
            return 0.04f - 0.001f * (elevation - 40.0f);
        }
        else if(elevation > 30.0f)
        {
            return 0.06f - 0.002f * (elevation - 30.0f);
        }
        else if(elevation > 20.0f)
        {
            return 0.12f - 0.006f * (elevation - 20.0f);
        }
        else if(elevation > 10.0f)
        {
            return 0.27f - 0.015f * (elevation - 10.0f);
        }
        else if(elevation > 5.0f)
        {
            return 0.42f - 0.030f * (elevation - 5.0f);
        }
        else if(elevation > 0.0f)
        {
            return 1.00f - 0.116f * (elevation - 0.0f);
        }
        else
        {
            return 1.00f;
        }
    }
}
