using System;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestWxModel : MonoBehaviour
{
    WeatherModel wxModel = new WeatherModel();
    Vector3 stDist = new Vector3(1, 0, 0);
    float rotationPeriod = 60 * 60 * 24; // seconds
    float seasonsPeriod = 60 * 60 * 24 * 365.24f; // seconds
    float seasonsAmplitude = 23.5f; // how far north the subsolar point is at the summer solstice, degrees of arc
    float t = 0.0f; // seconds
    float deltaT = 3600.0f; // seconds
    int testIterations = 20000;
    int batchSize = 100;
    bool runVis = true;
    bool runStrW = true;

    static string testId = "spawmWxTestA";

    public void RunTest()
    {
        FastTrig ft = new FastTrig(900);
        for(float i = 0; i < 90; i += 0.1f)
        {
            //UnityEngine.Debug.Log("Sin(" + i + ") = " + ft.Sin(i));
        }
        for(float i = 0; i < 1; i += 0.01f)
        {
            //UnityEngine.Debug.Log("Arcsin(" + i + ") = " + ft.Asin(i));
        }

        // wxModel.Step(60, stDist);
        // UpdateStDist(60);
        if(runStrW)
        {
            DirectoryInfo di = Directory.CreateDirectory("C:\\Users\\srira\\Desktop\\" + testId);
            di = Directory.CreateDirectory("C:\\Users\\srira\\Desktop\\" + testId + "\\tmpField");
            di = Directory.CreateDirectory("C:\\Users\\srira\\Desktop\\" + testId + "\\pwrIField");
            di = Directory.CreateDirectory("C:\\Users\\srira\\Desktop\\" + testId + "\\albField");
            
            string filenameTmpC = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpC.dat";
            string filenameTmpF = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpF.dat";
            string filenamePwrI = "C:\\Users\\srira\\Desktop\\" + testId + "\\pwrI.dat";
            string filenamePwrO = "C:\\Users\\srira\\Desktop\\" + testId + "\\pwrO.dat";
            string filenameAlb = "C:\\Users\\srira\\Desktop\\" + testId + "\\alb.dat";
            string filenameIce = "C:\\Users\\srira\\Desktop\\" + testId + "\\seaIce.dat";
            string filenameMltP = "C:\\Users\\srira\\Desktop\\" + testId + "\\meltPonds.dat";

            string[] filenameTmpFLat = new string[19];
            string[] filenameTmpFLatS = new string[19];

            for(int i = 0; i < 19; i++)
            {
                int lat = 10 * (i - 9);
                int latAbs = Mathf.Abs(lat);
                string sign = ((lat >= 0)?"N":"S");
                filenameTmpFLat[i] = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpF" + latAbs + sign + "-L.dat";
                filenameTmpFLatS[i] = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpF" + latAbs + sign + "-S.dat";
            }

            File.Delete(filenameTmpC);
            File.Delete(filenameTmpF);
            File.Delete(filenamePwrI);
            File.Delete(filenamePwrO);
            File.Delete(filenameAlb);
            File.Delete(filenameIce);
            File.Delete(filenameMltP);

            for(int i = 0; i < 19; i++)
            {
                File.Delete(filenameTmpFLat[i]);
                File.Delete(filenameTmpFLatS[i]);
            }

            using StreamWriter swTmpC = File.AppendText(filenameTmpC);
            using StreamWriter swTmpF = File.AppendText(filenameTmpF);
            using StreamWriter swPwrI = File.AppendText(filenamePwrI);
            using StreamWriter swPwrO = File.AppendText(filenamePwrO);
            using StreamWriter swAlb = File.AppendText(filenameAlb);
            using StreamWriter swIce = File.AppendText(filenameIce);
            using StreamWriter swMltP = File.AppendText(filenameMltP);

            StreamWriter[] swTmpFLat = new StreamWriter[19];
            StreamWriter[] swTmpFLatS = new StreamWriter[19];

            for(int i = 0; i < 19; i++)
            {
                swTmpFLat[i] = File.AppendText(filenameTmpFLat[i]);
                swTmpFLatS[i] = File.AppendText(filenameTmpFLatS[i]);
            }

            long millis = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            for(int i = 0; i < testIterations; i++)
            {
                wxModel.Step(deltaT, stDist);
                UpdateStDist(deltaT);

                swTmpC.WriteLine((t/rotationPeriod) + "\t" + (wxModel.potentialTemperature[16051] - 273.15));
                swTmpF.WriteLine((t/rotationPeriod) + "\t" + (1.8 * (wxModel.potentialTemperature[16051] - 273.15) + 32));
                swPwrI.WriteLine((t/rotationPeriod) + "\t" + wxModel.powerIn[48508 + 10]);
                swPwrO.WriteLine((t/rotationPeriod) + "\t" + wxModel.powerOut[48508 + 10]);
                swAlb.WriteLine((t/rotationPeriod) + "\t" + wxModel.albedo[48508 + 10]);
                swIce.WriteLine((t/rotationPeriod) + "\t" + wxModel.seaIce[48508 + 10]);
                swMltP.WriteLine((t/rotationPeriod) + "\t" + wxModel.seaIceMeltPonds[48508 + 10]);

                for(int j = 0; j < 19; j++)
                {
                    swTmpFLat[j].WriteLine((t/rotationPeriod) + "\t" + (1.8 * (wxModel.potentialTemperature[15928 + (j * 10)] - 273.15) + 32));
                    swTmpFLatS[j].WriteLine((t/rotationPeriod) + "\t" + (1.8 * (wxModel.potentialTemperature[48508 + (j * 10)] - 273.15) + 32));
                
                }

                if(i < testIterations && runVis) {
                    string tmpFieldFilename = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpField\\" + i.ToString().PadLeft(7, '0') + ".wxu";
                    string tmpFieldImgname = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpField\\" + i.ToString().PadLeft(7, '0') + ".png";
                    
                    string pwrIFieldFilename = "C:\\Users\\srira\\Desktop\\" + testId + "\\pwrIField\\" + i.ToString().PadLeft(7, '0') + ".wxu";
                    string pwrIFieldImgname = "C:\\Users\\srira\\Desktop\\" + testId + "\\pwrIField\\" + i.ToString().PadLeft(7, '0') + ".png";
                    
                    string albFieldFilename = "C:\\Users\\srira\\Desktop\\" + testId + "\\albField\\" + i.ToString().PadLeft(7, '0') + ".wxu";
                    string albFieldImgname = "C:\\Users\\srira\\Desktop\\" + testId + "\\albField\\" + i.ToString().PadLeft(7, '0') + ".png";
                    
                    using StreamWriter swTmpField = File.AppendText(tmpFieldFilename);
                    using StreamWriter swPwrIField = File.AppendText(pwrIFieldFilename);
                    using StreamWriter swAlbField = File.AppendText(albFieldFilename);

                    for(int j = 0; j < 360 * 181; j++) {
                        swTmpField.WriteLine(wxModel.mappingScheme[j].x + "\t" + wxModel.mappingScheme[j].y + "\t" + wxModel.potentialTemperature[j]);
                        swPwrIField.WriteLine(wxModel.mappingScheme[j].x + "\t" + wxModel.mappingScheme[j].y + "\t" + wxModel.powerIn[j]);
                        swAlbField.WriteLine(wxModel.mappingScheme[j].x + "\t" + wxModel.mappingScheme[j].y + "\t" + wxModel.albedo[j]);
                    }

                    if(i % batchSize == (batchSize - 1) && i >= batchSize)
                    {
                        for(int j = 0; j < batchSize; j++)
                        {
                            string tmpFieldFilename1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpField\\" + (i - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".wxu";
                            string tmpFieldImgname1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpField\\" + (i - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".png";
                            string tmpFieldLogname = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpField\\" + (i - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + "-log.txt";
                    
                            string pwrIFieldFilename1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\pwrIField\\" + (i - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".wxu";
                            string pwrIFieldImgname1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\pwrIField\\" + (i - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".png";

                            string albFieldFilename1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\albField\\" + (i - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".wxu";
                            string albFieldImgname1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\albField\\" + (i - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".png";

                            UnityEngine.Debug.Log("java.exe -jar \"D:\\Eclipse Workspaces\\StardeepWorkspace\\DrawFieldAsImage.jar\" " + tmpFieldFilename1 + " " + tmpFieldImgname1 + " tmp 360 181");
                            
                            var processInfo = new ProcessStartInfo("java.exe", "-jar \"D:\\Eclipse Workspaces\\StardeepWorkspace\\DrawFieldAsImage.jar\" " + tmpFieldFilename1 + " " + tmpFieldImgname1 + " tmp 360 181")
                                {
                                    CreateNoWindow = true,
                                    UseShellExecute = false
                                };
                                
                            StringBuilder sb = new StringBuilder();
                            Process proc;

                            if ((proc = Process.Start(processInfo)) == null)
                            {
                                UnityEngine.Debug.Log("Invalid Operation Exception");
                                throw new InvalidOperationException("??");
                            }

                            bool debug = false;
                            if(debug)
                            {
                                // redirect the output
                                proc.StartInfo.RedirectStandardOutput = true;
                                proc.StartInfo.RedirectStandardError = true;

                                // hookup the eventhandlers to capture the data that is received
                                proc.OutputDataReceived += (sender, args) => sb.AppendLine(args.Data);
                                proc.ErrorDataReceived += (sender, args) => sb.AppendLine(args.Data);

                                // direct start
                                proc.StartInfo.UseShellExecute=false;

                                proc.Start();
                                // start our event pumps
                                proc.BeginOutputReadLine();
                                proc.BeginErrorReadLine();
                            }

                            proc.WaitForExit();
                            int exitCode = proc.ExitCode;

                            UnityEngine.Debug.Log("Java DrawFieldAsImage Exit Code: " + exitCode);

                            UnityEngine.Debug.Log("java.exe -jar \"D:\\Eclipse Workspaces\\StardeepWorkspace\\DrawFieldAsImage.jar\" " + pwrIFieldFilename1 + " " + pwrIFieldImgname1 + " pwr 360 181");

                            processInfo = new ProcessStartInfo("java.exe", "-jar \"D:\\Eclipse Workspaces\\StardeepWorkspace\\DrawFieldAsImage.jar\" " + pwrIFieldFilename1 + " " + pwrIFieldImgname1 + " pwr 360 181")
                                {
                                    CreateNoWindow = true,
                                    UseShellExecute = false
                                };

                            if ((proc = Process.Start(processInfo)) == null)
                            {
                                UnityEngine.Debug.Log("Invalid Operation Exception");
                                throw new InvalidOperationException("??");
                            }

                            if(debug)
                            {
                                // do whatever you need with the content of sb.ToString();
                                using StreamWriter swCmdLog = File.AppendText(tmpFieldLogname);
                                swCmdLog.Write(sb.ToString());
                            }
                            
                            proc.WaitForExit();
                            exitCode = proc.ExitCode;

                            UnityEngine.Debug.Log("Java DrawFieldAsImage Exit Code: " + exitCode);
                            
                            UnityEngine.Debug.Log("java.exe -jar \"D:\\Eclipse Workspaces\\StardeepWorkspace\\DrawFieldAsImage.jar\" " + albFieldFilename1 + " " + albFieldImgname1 + " alb 360 181");

                            processInfo = new ProcessStartInfo("java.exe", "-jar \"D:\\Eclipse Workspaces\\StardeepWorkspace\\DrawFieldAsImage.jar\" " + albFieldFilename1 + " " + albFieldImgname1 + " alb 360 181")
                                {
                                    CreateNoWindow = true,
                                    UseShellExecute = false
                                };

                            if ((proc = Process.Start(processInfo)) == null)
                            {
                                UnityEngine.Debug.Log("Invalid Operation Exception");
                                throw new InvalidOperationException("??");
                            }

                            if(debug)
                            {
                                // do whatever you need with the content of sb.ToString();
                                using StreamWriter swCmdLog = File.AppendText(tmpFieldLogname);
                                swCmdLog.Write(sb.ToString());
                            }
                            
                            proc.WaitForExit();
                            exitCode = proc.ExitCode;
                            proc.Close();

                            UnityEngine.Debug.Log("Java DrawFieldAsImage Exit Code: " + exitCode);
                        }
                    }
                    
                    // string command = "-jar D:\\Eclipse Workspaces\\StardeepWorkspace\\DrawFieldAsImage.jar " + tmpFieldFilename + " " + tmpFieldImgname + " tmp 360 181";

                    // var startInfo = new ProcessStartInfo
                    // {            
                    //     FileName = "java.exe",
                    //     Arguments = "/C " + command,
                    //     WindowStyle = ProcessWindowStyle.Hidden,
                    //     CreateNoWindow = true,
                    //     UseShellExecute = false
                    // };

                    // Process.Start(startInfo);
                }
            }

            int ii = testIterations + 9;
            UnityEngine.Debug.Log(ii);
            if(runVis)
            {
                    for(int j = 0; j < batchSize; j++)
                    {
                        string tmpFieldFilename1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpField\\" + (ii - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".wxu";
                        string tmpFieldImgname1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpField\\" + (ii - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".png";
                        string tmpFieldLogname = "C:\\Users\\srira\\Desktop\\" + testId + "\\tmpField\\" + (ii - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + "-log.txt";

                        string pwrIFieldFilename1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\pwrIField\\" + (ii - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".wxu";
                        string pwrIFieldImgname1 = "C:\\Users\\srira\\Desktop\\" + testId + "\\pwrIField\\" + (ii - (batchSize - 1) - batchSize + j).ToString().PadLeft(7, '0') + ".png";

                        UnityEngine.Debug.Log("java.exe -jar \"D:\\Eclipse Workspaces\\StardeepWorkspace\\DrawFieldAsImage.jar\" " + tmpFieldFilename1 + " " + tmpFieldImgname1 + " tmp 360 181");
                        
                        var processInfo = new ProcessStartInfo("java.exe", "-jar \"D:\\Eclipse Workspaces\\StardeepWorkspace\\DrawFieldAsImage.jar\" " + tmpFieldFilename1 + " " + tmpFieldImgname1 + " tmp 360 181")
                            {
                                CreateNoWindow = true,
                                UseShellExecute = false
                            };
                            
                        StringBuilder sb = new StringBuilder();
                        Process proc;

                        if ((proc = Process.Start(processInfo)) == null)
                        {
                            UnityEngine.Debug.Log("Invalid Operation Exception");
                            throw new InvalidOperationException("??");
                        }

                        bool debug = false;
                        if(debug)
                        {
                            // redirect the output
                            proc.StartInfo.RedirectStandardOutput = true;
                            proc.StartInfo.RedirectStandardError = true;

                            // hookup the eventhandlers to capture the data that is received
                            proc.OutputDataReceived += (sender, args) => sb.AppendLine(args.Data);
                            proc.ErrorDataReceived += (sender, args) => sb.AppendLine(args.Data);

                            // direct start
                            proc.StartInfo.UseShellExecute=false;

                            proc.Start();
                            // start our event pumps
                            proc.BeginOutputReadLine();
                            proc.BeginErrorReadLine();
                        }

                        proc.WaitForExit();
                        int exitCode = proc.ExitCode;

                        if(debug)
                        {
                            // do whatever you need with the content of sb.ToString();
                            using StreamWriter swCmdLog = File.AppendText(tmpFieldLogname);
                            swCmdLog.Write(sb.ToString());
                        }
                        
                        proc.Close();

                        UnityEngine.Debug.Log("Java DrawFieldAsImage Exit Code: " + exitCode);
                    }
            }

            long millisN = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            UnityEngine.Debug.Log("Average time per step: "+ ((float) (millisN-millis)/testIterations) + " ms");

            for(int i = 0; i < 19; i++)
            {
                swTmpFLat[i].Close();
            }

            //Debug.Log("Model Step Skipped");
        } 
        else 
        {
            long millis = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            for(int i = 0; i < testIterations; i++)
            {
                wxModel.Step(deltaT, stDist);
                UpdateStDist(deltaT);
            }
            
            long millisN = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            UnityEngine.Debug.Log("Average time per step (NoStrW): "+ ((float) (millisN-millis)/testIterations) + " ms");
        }
    }

    StreamWriter swDecl = File.AppendText("C:\\Users\\srira\\Desktop\\" + testId + "\\decl.dat");

    private void UpdateStDist(float deltaT)
    {
        t += deltaT;

        // all seasons northern hemisphere
        // t/seasonsPeriod: 0 degrees = spring equinox, 90 = summer solstice
        float subsolarPtLat = Mathf.Rad2Deg * Mathf.Asin(Mathf.Sin(seasonsAmplitude*Mathf.Deg2Rad) * Mathf.Sin(t*360*Mathf.Deg2Rad/seasonsPeriod));
        float argumentOfRotation = -(t/rotationPeriod)*360; // 0 means prime meridian faces sun, 180 means faces away

        //Debug.Log("Latitude of Subsolar Point: " + subsolarPtLat + " degrees of arc");
        //Debug.Log("Argument of Rotation: " + argumentOfRotation + " degrees of arc");
        
        swDecl.WriteLine(t/rotationPeriod + "\t" + subsolarPtLat);
        stDist = new Vector3(Mathf.Cos(argumentOfRotation*Mathf.Deg2Rad) * Mathf.Cos(subsolarPtLat*Mathf.Deg2Rad), Mathf.Sin(subsolarPtLat*Mathf.Deg2Rad), Mathf.Sin(argumentOfRotation*Mathf.Deg2Rad) * Mathf.Cos(subsolarPtLat*Mathf.Deg2Rad));
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
